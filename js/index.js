var ctx = null;
var paddleL = {
	x : 0, 
	y : null,	
	width : 10,
	speed : 15,
	tall : 200
}
var paddleR = {
	x : null,
	y : null,		
	width : 10,
	speed : 15,
	tall : 200
}
var ball = {
	x : 300,
	y : 50,
	radius : 20,
	speed : 10
}
var keys = {

}
var angle = {
	big : 1.7,
	small : 1.4
}
var scorePlayerL = 0;
var scorePlayerR = 0;

ballUpRight = function () {
	ball.x = ball.x + ball.speed;
	ball.y = ball.y - ball.speed * 1.4;
}

ballDownRight = function () {
	ball.x = ball.x + ball.speed;
	ball.y = ball.y + ball.speed * 1.4;	
}

ballUpLeft = function () {
	ball.x = ball.x - ball.speed;
	ball.y = ball.y - ball.speed * 1.4;
}

ballDownLeft = function () {
	ball.x = ball.x - ball.speed;
	ball.y = ball.y + ball.speed * 1.4;	
}


// the initial move of the ball will be this
currentMove = ballUpLeft;

$( document ).ready(function() {
    var canvas = document.getElementById('canvas');
    canvas.width  = $( window ).width();
	canvas.height = $( window ).height();
	ctx = canvas.getContext('2d');
	paddleL.y = window.innerHeight/2; 
	paddleR.y = window.innerHeight/2; 
	paddleR.x = window.innerWidth - window.innerWidth*3/100; 
	
	setInterval(function(){ 
		draw();
		moveBall();
		movepaddles();
		checkScore();	
	}, 30);
	
});

$(document).keydown(function (e) {
	var code = e.keyCode || e.which;
    keys[code] = true;	    
});

$(document).keyup(function (e) {
	var code = e.keyCode || e.which;
    delete keys[code];    
});

function draw () {
	ctx.clearRect(0,0,window.innerWidth,window.innerHeight);
	drawpaddles();
	drawBall();
	printScore();	
}

function drawpaddles () {		
	ctx.fillRect(paddleL.x,paddleL.y,paddleL.width,paddleL.tall);
	ctx.fillRect(paddleR.x,paddleR.y,paddleR.width,paddleR.tall);
}

function drawBall () {	
    ctx.beginPath();
    ctx.arc(ball.x, ball.y, ball.radius, 0, 2 * Math.PI, false);    
    ctx.fill();
}

function paddleRUp() {
	if (paddleR.y >= 0) {
		paddleR.y = paddleR.y - paddleR.speed;
	}	
}

function paddleRDown () {
	if(paddleR.y + paddleR.tall <= window.innerHeight) {
		paddleR.y = paddleR.y + paddleR.speed;
	}
}

function paddleLUp () {	
	if (paddleL.y >= 0) {
		paddleL.y = paddleL.y - paddleL.speed;
	}
}

function paddleLDown () {
	if(paddleL.y + paddleL.tall <= window.innerHeight) {
		paddleL.y = paddleL.y + paddleL.speed;
	}
}

function movepaddles() {	
	 for (var code in keys) {        
	    if ( code == 80 ) { // in case P pressed
			paddleRUp();	
		} else if ( code == 76 ) { // in case L pressed
			paddleRDown();
		} else if ( code == 81 ) { // in case Q pressed
			paddleLUp();
		} else if ( code == 65 ) { // in case A pressed
			paddleLDown();
		}	    
    }	
}
	
function moveBall () {

	// when ball hit top or buttom
	if ( ball.y <= 0 + ball.radius && currentMove == ballUpRight ) { // in case the ball hits the top bar goin to right direction
		currentMove = ballDownRight;
	} else if ( ball.y + 50 >=  window.innerHeight && currentMove == ballDownRight ) { // in case the ball hits the buttom bar and direction is right
		currentMove = ballUpRight;		
	} else if ( ball.y - ball. radius <= 0 && currentMove == ballUpLeft ) { // in case the ball hits the top bar goin to left direction
		currentMove = ballDownLeft;
	} else if ( ball.y + 50 >=  window.innerHeight && currentMove == ballDownLeft ) { // in case the ball hits the buttom bar and direction is left
		currentMove = ballUpLeft;	
	}	

	// when ball hits right paddle
	if ( paddleR.y <= ball.y && ball.y <= ( paddleR.y + paddleR.tall/2 ) && ball.x+ball.radius >= paddleR.x ) { // in case ball  touches right paddle top half
			currentMove = ballUpLeft;			
	}	else if (paddleR.y + paddleR.tall / 2 <= ball.y && ball.y <= ( paddleR.y + paddleR.tall ) && ball.x+ball.radius >= paddleR.x ) { // in case ball  touches right paddle botom half
			currentMove = ballDownLeft; 
	}	else if (paddleL.y <= ball.y && ball.y <= ( paddleL.y + paddleL.tall/2 ) && ball.x-ball.radius <= paddleL.x ) { // in case ball  touches left paddle top half
			currentMove = ballUpRight;
	} 	else if (paddleL.y + paddleL.tall / 2 <= ball.y && ball.y <= ( paddleL.y + paddleL.tall ) && ball.x-ball.radius <= paddleL.x ) { // in case ball touches left paddle botom half
			currentMove = ballDownRight; 
	}

	currentMove()	 	
}

function randomizer1 () {
	return Math.random() * ( angle.big - angle.small ) + angle.small
}

function randomizer2 () {
	return Math.random() * ( angle.small - 1 ) + 1
}

function printScore () {
	ctx.font = "30px Arial";
	ctx.fillText("Player L " + scorePlayerL ,window.innerWidth *10/100,50);
	ctx.fillText("Player R " + scorePlayerR ,window.innerWidth *80/100,50);
}

function checkScore () {
	if ( ball.x < 0 ) {
		scorePlayerR = scorePlayerR + 1;			
		middlefielddKick();			
	} else if ( ball.x > window.innerWidth ) {
		scorePlayerL = scorePlayerL + 1;
		middlefielddKick();
	}
}

function middlefielddKick () {
	ball.x = window.innerWidth/2;
	ball.y = window.innerHeight/2;
	var rand = Math.random() * (3 - 0) + 0;
	switch(rand) {
	    case 0:
	        currentMove = ballDownRight;
	        break;
	    case 1:
	        currentMove = ballDownLeft;
	        break;
	    case 2:
	        currentMove = ballUpRight;
	        break;
	    case 3:
	        currentMove = ballUpLeft;
	        break;	    
	}
}