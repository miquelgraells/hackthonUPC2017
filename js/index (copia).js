var ctx = null;
var paleL = {
	x : 0, 
	y : 500,	
	width : 10,
	speed : 10,
	tall : 200
}
var paleR = {
	x : 1800,
	y : 500,		
	width : 10,
	speed : 10,
	tall : 200
}
var ball = {
	x : 300,
	y : 50,
	radius : 20,
	speed : 10,
	cosAngleAgeY : 1.7
}
var keys = {};


ballUpRight = function () {
	ball.x = ball.x + ball.speed;
	ball.y = ball.y - ball.speed * ball.cosAngleAgeY;
}

ballDownRight = function () {
	ball.x = ball.x + ball.speed;
	ball.y = ball.y + ball.speed * ball.cosAngleAgeY;	
}

ballUpLeft = function () {
	ball.x = ball.x - ball.speed;
	ball.y = ball.y - ball.speed;
}

ballDownLeft = function () {
	ball.x = ball.x - ball.speed;
	ball.y = ball.y + ball.speed * ball.cosAngleAgeY;	
}

// the initial move of the ball will be this
currentMove = ballDownRight;

$( document ).ready(function() {
    var canvas = document.getElementById('canvas');
    canvas.width  = $( window ).width();
	canvas.height = $( window ).height();
	ctx = canvas.getContext('2d');
	
	setInterval(function(){ 
		draw();
		movePales();
	}, 30);
	
});

$(document).keydown(function (e) {
	var code = e.keyCode || e.which;
    keys[code] = true;	    
});

$(document).keyup(function (e) {
	var code = e.keyCode || e.which;
    delete keys[code];    
});

function draw () {
	ctx.clearRect(0,0,window.innerWidth,window.innerHeight);
	drawPales();
	drawBall();
	moveBall();
}

function drawPales () {		
	ctx.fillRect(paleL.x,paleL.y,paleL.width,paleL.tall);
	ctx.fillRect(paleR.x,paleR.y,paleR.width,paleR.tall);
}

function drawBall () {	
    ctx.beginPath();
    ctx.arc(ball.x, ball.y, ball.radius, 0, 2 * Math.PI, false);    
    ctx.fill();
}

function paleRUp() {
	if (paleR.y >= 0) {
		paleR.y = paleR.y - paleR.speed;
	}	
}

function paleRDown () {
	if(paleR.y + paleR.tall <= window.innerHeight) {
		paleR.y = paleR.y + paleR.speed;
	}
}

function paleLUp () {	
	if (paleL.y >= 0) {
		paleL.y = paleL.y - paleL.speed;
	}
}

function paleLDown () {
	if(paleL.y + paleL.tall <= window.innerHeight) {
		paleL.y = paleL.y + paleL.speed;
	}
}

function movePales() {	
	 for (var code in keys) {        
	    if ( code == 80 ) { // in case P pressed
			paleRUp();	
		} else if ( code == 76 ) { // in case L pressed
			paleRDown();
		} else if ( code == 81 ) { // in case Q pressed
			paleLUp();
		} else if ( code == 65 ) { // in case A pressed
			paleLDown();
		}	    
    }	
}
	
function moveBall () {
	// when ball hit top or buttom
	if ( ball.y <= 0 && currentMove == ballUpRight ) { // in case the ball hits the top bar goin to right direction
		currentMove = ballDownRight;
	} else if ( ball.y >=  window.innerHeight && currentMove == ballDownRight ) { // in case the ball hits the buttom bar and direction is right
		currentMove = ballUpRight;		
	} else if ( ball.y <= 0 && currentMove == ballUpLeft ) { // in case the ball hits the top bar goin to left direction
		currentMove = ballDownLeft;
	} else if ( ball.y >=  window.innerHeight && currentMove == ballDownLeft ) { // in case the ball hits the buttom bar and direction is left
		currentMove = ballUpLeft;	
	}

	// when ball hits right pale
	if ( paleR.y <= ball.y && ball.y <= ( paleR.y + paleR.tall/4 ) && ball.x == paleR.x ) { // in case ball  touches right pale firs top half
			ball.cosAngleAgeY = randomizer1 ();
			currentMove = ballUpLeft;

			/*ball.x = ball.x - ball.speed;
			ball.y = ball.y - ball.speed * randomIncrement1;*/

			

			

	// } else if () { // in case ball  touches right pale second top half

	// } else if () { // in case ball  touches right pale first buttom half

	// } else if () { // in case ball  touches right pale second buttom half

	// }

	// when ball hits left pale
	// if () { // in case ball  touches left pale firs top half

	// } else if () { // in case ball  touches left pale second top half

	// } else if () { // in case ball  touches left pale first buttom half

	// } else if () { // in case ball  touches left pale second buttom half

	}

	/*if ( (paleR.y <= ball.y) &&  (paleR.tall + paleR.y >= ball.y) && (ball.x == paleR.x)) {	
		alert("lorem");
	}*/


	currentMove()	 	
}

function randomizer1 (){
	return Math.random() * ( 1,7 + 1,7 ) - 1,7
}